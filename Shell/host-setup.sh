#!/bin/bash

#################################################
#
# This script sets the hostname and adds DNS
# entries as per the requested hostname using
# the DNS API from PowerDNS
#
# Author : nitinb@acceldata.io
VERSION=0.1
#
#################################################

A1=$1
MYHOSTNAME=$A1
MYHOSTIP=$(/usr/bin/hostname -i)
DNSHOST="10.90.5.52"

if [[ $# -eq 0 ]] ; then
  echo "USAGE : ./host-setup.sh <hostname>"
  echo " "
  echo "NOTE : You don't have to provide an FQDN. Just the name of the host"
  echo "Like hdp1000, pulse1010. No need for hdp1000.sre.iti.acceldata.dev"
  echo "You didn't pass any hostname. Cannot proceed!"
  exit 2
fi

while [[ "$A1" =~ ^([a-z]+)([0-9]+) ]]; do
  HOSTNUM=${BASH_REMATCH[2]}
  A1="${A1:${#BASH_REMATCH[0]}}"
done

if [ ${HOSTNUM} -le 9999 ] && [ ${HOSTNUM} -ge 1000 ]; then
  echo "The hostname : $MYHOSTNAME, seems to be in proper format. Proceeding to check the availability ..."
else
  echo "Sorry, hostname have exactly 4 digits please..."
  exit 4
fi

#(( HOSTNUM <= 9999 && HOSTNUM >= 1000)) || echo "Sorry, hostname cannot have more than 4 digits please..."

#echo "The hostname : $MYHOSTNAME, seems to be in proper format. Proceeding to check the availability ..."

MYVLAN=$(echo ${MYHOSTIP} | /usr/bin/awk -F. '{print $3}')

case "$MYVLAN" in
	2) echo "Instance is in Management VLAN"
	   MYFQDN=${MYHOSTNAME}.mgmt.iti.acceldata.dev.
	   ;;
	3) echo "Instance is in Pulse VLAN"
	   MYFQDN=${MYHOSTNAME}.pulse.iti.acceldata.dev.
	   ;;
	4) echo "Instance is in Torch VLAN"
	   MYFQDN=${MYHOSTNAME}.torch.iti.acceldata.dev.
	   ;;
	5) echo "Instance is in Operations VLAN"
	   MYFQDN=${MYHOSTNAME}.ops.iti.acceldata.dev.
	   ;;
	6) echo "Instance is in SRE VLAN"
	   MYFQDN=${MYHOSTNAME}.sre.iti.acceldata.dev.
	   ;;
	7) echo "Instance is in QE VLAN"
	   MYFQDN=${MYHOSTNAME}.qe.iti.acceldata.dev.
	   ;;
	*) echo "I'm not sure which VLAN this is. Using Default"
	   MYFQDN=${MYHOSTNAME}.def.iti.acceldata.dev.
	   ;;
esac

## Check if hostname is available

$(/usr/bin/host $MYFQDN > /dev/null)
if [ $? -eq '0' ]; then
  echo "Sorry, the host $MYFQDN already exists in DNS. Cannot proceed ..."
  exit 3
else
  echo "Cannot find $MYFQDN on DNS query. Checking further on DNS Server"
fi

## Next adding the entry to DNS

/usr/bin/curl -v -H 'X-API-Key: ADdns@p1key' -X PATCH --data '{"rrsets": [ {"name": "'$MYFQDN'", "type": "A", "ttl": 3600, "changetype": "REPLACE", "records": [ {"content": "'$MYHOSTIP'", "disabled": false } ] } ] }' http://10.90.5.52:8081/api/v1/servers/localhost/zones/acceldata.dev. | /usr/bin/jq .

MYFQDN=$(echo $MYFQDN | /usr/bin/rev | /usr/bin/cut -d. -f2- | /usr/bin/rev)

echo "Now I will run the below commands ..."
echo "hostnamectl set-hostname --static $MYFQDN"
/usr/bin/hostnamectl set-hostname --static $MYFQDN
echo "hostname $MYFQDN"
/usr/bin/hostname $MYFQDN
/usr/bin/hostname
/usr/bin/host $MYFQDN 10.90.5.52 | /usr/bin/grep address
