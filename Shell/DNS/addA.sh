#!/bin/bash

###########################################
#
# ACCELDATA ITI DNS
# A simple wrapper to add A RECORD to DNS
#
# addA.sh <hostname> <IP Address>
#
# Author : nitinb
###########################################

source ./config

HOSTNAME=${1}.
IPADDRESS=$2

echo "Adding host : ${HOSTNAME} with IP : ${IPADDRESS} to axl.iti zone"

echo $CURL -s -H 'X-API-Key: '"$APIKEY" -H 'Content-Type: application/json' -X PATCH --data '{"rrsets": [ {"name": "'${HOSTNAME}'", "type": "A", "ttl": 3600, "changetype": "REPLACE", "records": [ {"content": "'$IPADDRESS'", "disabled": false } ] } ] }' http://${DNSHOST}:${DNSPORT}/api/v1/servers/localhost/zones/axl.iti
