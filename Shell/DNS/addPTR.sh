#!/bin/bash

###########################################
#
# ACCELDATA ITI DNS
# A simple wrapper to add PTR RECORD to DNS
#
# addPTR.sh <hostname> <IP Address>
#
# Author : nitinb
###########################################

source ./config

HOSTNAME=${1}.
IPADDRESS=$2

echo "Adding host : ${HOSTNAME} with IP : ${IPADDRESS} to 90.10.in-addr.arpa. zone"

IPENTRY=$(echo $IPADDRESS | awk -F. '{print $4"."$3".90.10.in-addr.arpa."}')
echo $CURL -s -H 'X-API-Key: '"$APIKEY" -H 'Content-Type: application/json' -X PATCH --data '{"rrsets": [ {"name": "'${IPENTRY}'", "type": "A", "ttl": 3600, "changetype": "REPLACE", "records": [ {"content": "'$HOSTNAME'", "disabled": false } ] } ] }' http://${DNSHOST}:${DNSPORT}/api/v1/servers/localhost/zones/90.10.in-addr.arpa.
