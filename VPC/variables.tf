region = "us-east-1"
vpc-name   = "ad1-va1"
cidr-range =  "10.10.0.0/18"
azs = ["us-east-1a", "us-east-1b"]
pvt-subnet = "10.10.0.0/20"
pub-subnet = "10.10.20.0/27"
env-name = "ad1.va1.acceldata.dev"
