provider "aws" {
  region = "var.region"
}

resource "aws_vpc" "ad1-va1" {

#  name = "${vars.name}"
  cidr_block = "${var.cidr-range}"

#  azs             = ["vars.az"]
#  private_subnets = ["vars.pvt-subnet"]
#  public_subnets  = ["vars.pub-subnet"]

#  enable_nat_gateway = true

  tags = {
    Environment = "vars.env-name"
  }
}

resource "aws_internet_gateway" "igw-ad1" {
  vpc_id = "aws_vpc.${vars.name}.id"
  tags = {
    Name = "IGW - ad1"
  }
}

resource "aws_route_table" "rtbl1-ad1" {
  vpc_id = "${vars.name}.id"

  route = {
    cidr_block = "vars.pub-subnet"
    gateway_id = "${aws_internet_gateway.igw-ad1.id}"
  }

  tags = {
    Name = "RTBL1 - ad1"
  }
}

