import boto3

##############################################
# Simple Boto3 script to define EBS Volumes
# Ensure this runs only on Python 3
# This needs you to have AWS CLI access
# 
# Code written by - nitinb@acceldata.io
#
# Version - 1.0
#
##############################################

ec2 = boto3.client('ec2')  

#ADD YOUR AWS ACCESS KEY BELOW BETWEEN THE QUOTES
access_key = ""

#ADD YOUR AWS SECRET KEY BELOW BETWEEN THE QUOTES
secret_key = ""

vols_to_delete = list()

client = boto3.client('ec2', aws_access_key_id=access_key, aws_secret_access_key=secret_key)
#client = boto3.client('ec2', aws_access_key_id=access_key, aws_secret_access_key=secret_key,region_name='us-east-1')

ec2_regions = [region['RegionName'] for region in client.describe_regions()['Regions']]

print ("Volume-ID", "Vol state", "Region", sep=",")

for region in ec2_regions:
    conn = boto3.resource('ec2', aws_access_key_id=access_key, aws_secret_access_key=secret_key,
                   region_name=region)
    volume_iterator = conn.volumes.all()
    for vol in volume_iterator:
        if str(vol.state) == "available":
            vols_to_delete.append(vol.id)
            try:
                print ("trying to delete Volume ID : " + vol.id + " in " + region + " region")
                myoutput = vol.delete(
                    DryRun=False
                )

            except Exception as err:
                print ("Deleting volume-ID: " + vol.id + " failed due to error : " + str(err) )
           
    
