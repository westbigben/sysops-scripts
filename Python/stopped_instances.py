import boto3

##############################################
# Simple Boto3 script to define EC2 instances
# Ensure this runs only on Python 3
# This needs you to have AWS CLI access
# 
# Code written by - nitinb@acceldata.io
#
# Version - 1.0
#
##############################################

ec2 = boto3.client('ec2')  

#ADD YOUR AWS ACCESS KEY BELOW BETWEEN THE QUOTES
access_key = ""

#ADD YOUR AWS SECRET KEY BELOW BETWEEN THE QUOTES
secret_key = ""

client = boto3.client('ec2', aws_access_key_id=access_key, aws_secret_access_key=secret_key,region_name='us-east-1')

ec2_regions = [region['RegionName'] for region in client.describe_regions()['Regions']]

print ("Stopped Instance-ID", "Private-IP", "Public-IP", "Region", sep=",")

for region in ec2_regions:
    conn = boto3.resource('ec2', aws_access_key_id=access_key, aws_secret_access_key=secret_key,
                   region_name=region)
    instances = conn.instances.filter()
    for instance in instances:
        if instance.state["Name"] == "stopped":
             print (instance.id, instance.private_ip_address, instance.public_ip_address, region, sep=",")